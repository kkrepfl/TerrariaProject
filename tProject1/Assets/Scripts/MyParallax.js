﻿#pragma strict
public var background : Transform;
public var backgroundScale : Vector3;

public var midground : Transform;
public var midgroundScale : Vector3;

private var lastPosition : Vector3;

function Start () {
    lastPosition = transform.position;
}

function Update () {
    var move = transform.position - lastPosition;
    background.position += Vector3.Scale(move, backgroundScale);
    midground.position += Vector3.Scale(move, midgroundScale);
    
    lastPosition = transform.position;
}
