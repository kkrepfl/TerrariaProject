﻿#pragma strict

private var facingRight = true;
private var jump = false;
public var moveForce : float = 365f;
public var maxSpeed : float = 5f;
public var jumpForce : float = 1000f;
public var groundCheck : Transform;
public var speed : float;
public var gravity : float;
public var jumpSpeed : float;

private var grounded : boolean;
private var rb2d : Rigidbody2D;
private var lastPosition : Vector3;
private var startPosition : Vector3;
private var jumpHeight : int = 2;
private var verticalSpeed : float;
private var animator :Animator;

function Start() {

    grounded = false;
    rb2d = gameObject.GetComponent(Rigidbody2D) as Rigidbody2D;
    lastPosition = transform.position;
    startPosition = transform.position;
    animator = GetComponent(Animator);
}

function Update() {
 
    // Check to see if the character is touching the ground
    grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
    if (grounded)
    {
        jumpHeight = 2;
    }

    // Jump only if character is on the ground ... you'll need to adjust this if you want double jump
    if (Input.GetKeyDown(KeyCode.Space) && jumpHeight > 0){
        GetComponent(Rigidbody2D).velocity = new Vector2 (0,2.5);
        jumpHeight -= 1; 

    }
    var delta = Input.GetAxis("Horizontal") * speed;
    transform.position.x += delta *Time.deltaTime;

    //animator.SetFloat("Speed", Mathf.Abs(delta));

    if(grounded) {
        if(Input.GetButton("Jump")) {
            //animator.SetTrigger("jump");
            grounded = false;
            verticalSpeed = jumpSpeed;
        }
    }
    
    else{
        verticalSpeed -= gravity * Time.deltaTime;
        transform.position.y += verticalSpeed * Time.deltaTime;
    }

    animator.SetBool("Grounded", grounded);
}

function OnCollisionStay2D (other : Collision2D)
    {
        verticalSpeed = 0;
        grounded = true;
    } 

function OnCollisionExit2D (other : Collision2D)
    {
        grounded = false;
    }

function FixedUpdate() {
    // Determine if player is moving character to the left or right
    var h = Input.GetAxis("Horizontal");

    // Apply force to move character in desired direction, not exceeding the max speed
    if (h * rb2d.velocity.x < maxSpeed)
        rb2d.AddForce(Vector2.right * h * moveForce);

    if (Mathf.Abs (rb2d.velocity.x) > maxSpeed)
        rb2d.velocity = new Vector2(Mathf.Sign (rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);

   // Debug.Log(Mathf.Abs(rb2d.velocity.x));
    animator.SetFloat("Speed", Mathf.Abs(rb2d.velocity.x));

    // Flip character sprite if switching direction
    if (h > 0 && !facingRight)
        Flip ();
    else if (h < 0 && facingRight)
        Flip ();


 
}

function Flip() {
    facingRight = !facingRight;
    var theScale = transform.localScale;
    theScale.x *= -1;
    transform.localScale = theScale;
}